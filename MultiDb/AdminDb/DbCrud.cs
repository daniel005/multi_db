﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MultiDb.Models.InformacionUsuarios;
using MultiDb.Models;

namespace MultiDb.AdminDb
{
    public class DbCrud
    {
        //SELECT
        public List<Usuario> GetUsuarios()
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            List<Usuario> usuarios = connection.Query<Usuario>("SELECT * FROM usuarios WHERE eliminado='no';").ToList();
            return usuarios;
        }

        public List<Telefono> GetTelefonos()
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            List<Telefono> telefonos = connection.Query<Telefono>("SELECT * FROM telefonos WHERE eliminado='no';").ToList();
            return telefonos;
        }
        public List<Direccion> GetDirecciones()
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            List<Direccion> direcciones = connection.Query<Direccion>("SELECT * FROM direcciones WHERE eliminado='no';").ToList();
            return direcciones;
        }

        //insert
        public int InsertUsuario(Usuario usuario)
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            int resultado = connection.Execute("INSERT INTO usuarios(primer_nombre,segundo_nombre,apellido_paterno,apellido_materno,cedula,correo, usuario, password) values(@PrimerNombre,@SegundoNombre,@ApellidoPaterno,@ApellidoMaterno,@Cedula,@Correo,@CuentaUsuario,@Password)", usuario);
            return resultado;
        }
        public int InsertTelefono(Telefono telefono)
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            int resultado = connection.Execute("INSERT INTO telefonos(numero,id_usuario) values(@Numero,@IdUsuario)", telefono);
            return resultado;
        }
        public int InsertDireccion(Direccion direccion)
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            int resultado = connection.Execute("INSERT INTO direcciones(calle,numero,sector,ciudad,pais,id_usuario) values(@Calle,@Numero,@Sector,@Ciudad,@Pais,@IdUsuario)", direccion);
            return resultado;
        }

        //update
        public Usuario GetUsuario(int id)
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            Usuario datos = connection.QueryFirstOrDefault<Usuario>("SELECT * FROM usuarios WHERE id_usuario = @id", new { id});
            return datos;
        }
        public int UpdateUsuario(int id)
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            int resultado = connection.Execute("UPDATE usuarios SET primer_nombre=@PrimerNombre, segundo_nombre=@SegundoNombre, apellido_paterno=@ApellidoPaterno, apellido_materno=@ApellidoMaterno, cedula=@Cedula, correo=@Correo, usuario=@CuentaUsuario, password=@Password WHERE id_usuario=@id", new {id} );
            return resultado;
        }

        public Telefono GetTelefono(int id)//id usuario?
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            Telefono datos = connection.QueryFirstOrDefault<Telefono>("SELECT * FROM telefonos WHERE id_usuario = @id", new { id });
            return datos;
        }
        public int UpdateTelefono(int id)//id usuario?
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            int resultado = connection.Execute("UPDATE telefonos SET numero=@Numero WHERE id_usuario=@id", new { id });
            return resultado;
        }

        public Direccion GetDireccion(int id)//id usuario?
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            Direccion datos = connection.QueryFirstOrDefault<Direccion>("SELECT * FROM direcciones WHERE id_usuario = @id", new { id });
            return datos;
        }
        public int UpdateDireccion(int id)//id usuario?
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            int resultado = connection.Execute("UPDATE direcciones SET calle=@Calle, numero=@Numero, sector=@Sector, ciudad=@Ciudad, pais=@Pais WHERE id_usuario=@id", new { id });
            return resultado;
        }

        //delete
        public int DeleteUsuario(int id)
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            int resultado = connection.Execute("UPDATE usuarios SET eliminado = 'si' WHERE id_usuario = @id", new { id});
            return resultado;
        }

        public int DeleteTelefono(int id)//id usuario?
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            int resultado = connection.Execute("UPDATE telefonos SET eliminado = 'si' WHERE id_usuario = @id", new { id});
            return resultado;
        }

        public int DeleteDireccion(int id)//id usuario?
        {
            var connection = MultiDb.AdminDb.DbConnection.GetConnection();
            int resultado = connection.Execute("UPDATE direcciones SET eliminado = 'si' WHERE id_usuario =@id", new { id});
            return resultado;
        }
    }
}
