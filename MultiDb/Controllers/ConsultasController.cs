﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using MultiDb.Models;
using System.Data.Common;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using MultiDb.Models.UsersDb;

namespace MultiDb.Controllers
{
    public class ConsultasController : Controller
    {
        MockConsulta consulta = new MockConsulta();
        
        [HttpGet]
        public IActionResult Consulta  (DbConnection connection , Engineers engineers)
        {
          
            List<string> database = consulta.ShowDatabases(connection);
            ViewBag.databases = database;
            foreach (string databaseName in database)
            {
                consulta.UseDatabase(connection, databaseName);
                List<string> table = consulta.ShowTables(connection);
                ViewBag.tables = table;
                foreach (string tableName in table)
                {
                    ViewBag.columns = consulta.ShowColumns(connection, tableName);
                }
            }
            return View();
        }
    }
}