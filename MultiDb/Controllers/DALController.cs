﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MultiDb.Models.UsersDb;
using Microsoft.Extensions.Configuration;

namespace MultiDb.Controllers
{
    public class DALController : Controller
    {
        private readonly IConfiguration configuration;
        public DALController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public IActionResult Index()
        {
            //ViewBag.port = port;
            //ViewBag.username = username;
            //ViewBag.password = password;
            return View();
        }

        public IActionResult Formsql(Engineers engineers)
        {
            DALController dp = new DALController(configuration);
            return View(engineers);
        }

        

        public IActionResult Formoracle(Engineers engineers)
        { 

            DALController dt = new DALController(configuration);
            return View(engineers);
        }
    }
}