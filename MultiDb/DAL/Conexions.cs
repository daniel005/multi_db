﻿using System.Data.SqlClient;
using System.Data.Common;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using MultiDb.Models.UsersDb;

namespace proyecto.DAL
{
    public enum DB
    {
        SQLServer = 1,
        MysqlServer = 2,
        Oracle = 3,
        MariaDB = 4,
    }
    public class Conexions //: DbContext 
    {
        SqlCommand cmd;
        //public Conexions (DbContextOptions<Conexions> options, DB dB) : base(options)
        //{

        //}

        //protected override void OnConfiguring(DbContextOptionsBuilder dbContextOptions)
        //{
        //    if(dbContextOptions != null)
        //    {
        //        var con = GetConnection(DB.Oracle);


        //    }
        // }
        public static DbConnection GetConnection(Engineers model , DB MMDB = DB.Oracle)
        {
            DbConnection connection = null;

            var byUserName = string.IsNullOrEmpty(model.username) || String.IsNullOrEmpty(model.password) ? "integrated security = no; " :
                String.Format("user ID={0} ,pwd={1};", model.username, model.password);

            var Conn = String.Format("data source={0};", model.ConnectionString, model.Database, byUserName);

            using(var con = connection.CreateCommand())
            {
                connection.Open();
                Console.WriteLine("Reading");
                con.CommandText = "";

                using(var reader = con.ExecuteReader())
                {
                    Console.WriteLine("tables");

                    var data = new DataTable();
                    data.Load(reader);
                    DataRow r = data.Rows[0];

                    foreach (var item in r.ItemArray)
                    {
                        Console.WriteLine(r);

                    }
                    con.Connection.Close();
                }
                return connection;
            }
        }
        public Conexions()
        {
            GetConnection( new Engineers()
            {
                username = ""
            }, 
            DB.MysqlServer);
        }

        public static DbConnection GetConection(Engineers model, DB MMDB = DB.SQLServer)
        {
            DbConnection connection = null;

            var ByuserNameorWindowsAtutentification = String.IsNullOrEmpty(model.username) || String.IsNullOrEmpty(model.password) ? "Integrated Security = True;" :
                String.Format("uid={0};pwd={1};", model.username, model.password);

            var byUserName = String.IsNullOrEmpty(model.username) || String.IsNullOrEmpty(model.password) ? "integrated security = no; " :
                String.Format("user ID={0} ,pwd={1};", model.username, model.password);

            var Con = String.Format("server={0};database={1};{2}", model.ConnectionString, model.Database, ByuserNameorWindowsAtutentification);
            var Conn = String.Format("data source={0};", model.ConnectionString, model.Database, byUserName); 
            switch (MMDB)
            {
                case DB.SQLServer:
                    connection = new SqlConnection(Con);
                    break;

                case DB.MysqlServer:
                    connection = new MySqlConnection("Server=127.0.0.1;database=registro;uid=root=;pwd=;");
                    break;
                    
                case DB.Oracle:
                    connection = new Oracle.ManagedDataAccess.Client.OracleConnection(Conn);
                    break;

                case DB.MariaDB:
                    connection = new MySqlConnection("server=localhost;userid=root;database=registro");
                    break;

                default:
                    break;
            }

            using(var com = connection.CreateCommand())
            {
                connection.Open();
                Console.WriteLine("Reading");
                com.CommandText = "Select * from [dbo].[person]";

                using (var reader = com.ExecuteReader())
                {
                    Console.WriteLine("Tables:");

                    var dat = new DataTable();
                    dat.Load(reader);
                    DataRow g = dat.Rows[0];

                    foreach (var item in g.ItemArray)
                    {
                        Console.WriteLine(g);
                    }
                }
                com.Connection.Close();
            }
            return connection;
        }
    }
}