﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultiDb.Models
{
    public class Consulta
    {
        //basics
        public string Select { get; set; }
        public string All { get; set; }
        public string From { get; set; }
        public string Where { get; set; }

        //operators in where clause
        public string Equal { get; set; }
        public string GreaterThan { get; set; }
        public string LessThan { get; set; }
        public string GreaterThanOrEqual { get; set; }
        public string LessThanOrEqual { get; set; }
        public string NotEqual { get; set; }

        //conjuctions for two or more conditions
        public string And { get; set; }
        public string Or { get; set; }
        public string Not { get; set; }

        //Order by clause
        public string OrderBy { get; set; }
        public string Ascending { get; set; }
        public string Descending { get; set; }

        //specified the number of records to return
        public string Top { get; set; } //SQL Server or MS Access
        public string Limit { get; set; }//MySQL
        public string Rownum { get; set; }//Oracle 

        //interaction with the selected column
        public string Min { get; set; }//returns the smallest value of the selected column
        public string Max { get; set; }//returns the largest value of the selected column
        public string Count { get; set; }//returns the number of rows that matches the criteria
        public string Avg { get; set; }//returns the average value of a numeric column
        public string Sum { get; set; }//returns the total sum of a numeric column

        //search for a specified pattern in a column 
        public string Like { get; set; }
        public string PercentWildcard { get; set; }//other database management systems
        public string AsteriskWildcard { get; set; }//MS Access

        //to specify multiple values in a where clause
        public string In { get; set; }

        //to select values within a given range, begin and end values are included
        public string Between { get; set; }

        //aliases, to change names
        public string  As { get; set; }
        public string Concat { get; set; }//MySQL, to concat columns

        //joins 
        public string InnerJoin { get; set; }
        public string LeftJoin { get; set; }
        public string RightJoin { get; set; }
        public string FullOuterJoin { get; set; }
        public string On { get; set; }

        //to combine the result-set of two or more select statement
        public string Union { get; set; }//select only distinct values
        public string UnionAll { get; set; }//select all including duplicated values



    }
}
