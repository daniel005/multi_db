﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultiDb.Models
{
    public class ConsultaParts 
    {
        //basics
        private string Select = " SELECT ";
        private string All = " * ";
        private string From = " FROM ";
        private string Where = " WHERE";

        //operators in where clause
        private string Equal = " = ";
        private string GreaterThan = " > ";
        private string LessThan = " < ";
        private string GreaterThanOrEqual = " >= ";
        private string LessThanOrEqual = " <= ";
        private string NotEqual = " <> ";

        //conjuctions for two or more conditions
        private string And = " AND ";
        private string Or = " OR ";
        private string Not = " NOT ";

        //Order by clause
        private string OrderBy = " ORDER BY ";
        private string Ascending = " ASC ";
        private string Descending = " DESC ";

        //specified the number of records to return
        private string Top = " TOP ";//SQL Server or MS Access
        private string Limit = " LIMIT ";//MySQL
        private string Rownum = " ROWNUM ";//Oracle 

        //interaction with the selected column
        private string Min = " MIN ";//returns the smallest value of the selected column
        private string Max = " MAX ";//returns the largest value of the selected column
        private string Count = " COUNT ";//returns the number of rows that matches the criteria
        private string Avg = " AVG ";//returns the average value of a numeric column
        private string Sum = " SUM ";//returns the total sum of a numeric column

        //search for a specified pattern in a column 
        private string Like = " LIKE ";
        private string PercentWildcard = "%";//other database management systems
        private string AsteriskWildcard = "*";//MS Access

        //to specify multiple values in a where clause
        private string In = " IN ";

        //to select values within a given range, begin and end values are included
        private string Between = " BETWEEN ";

        //aliases, to change names
        private string As = " AS ";
        private string Concat = " CONCAT ";//MySQL, to concat columns

        //joins 
        private string InnerJoin = " INNER JOIN ";
        private string LeftJoin = " LEFT JOIN ";
        private string RightJoin = " RIGHT JOIN ";
        private string FullOuterJoin = " FULL OUTER JOIN ";
        private string On = " ON ";

        //to combine the result-set of two or more select statement
        private string Union = " UNION ";//select only distinct values
        private string UnionAll = " UNION ALL ";//select all including duplicated values
    }
}
