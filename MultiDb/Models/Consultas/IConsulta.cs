﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultiDb.Models
{
    public interface IConsulta
    {
        IConsulta ExecuteQuery(string query);

        IConsulta ShowDatabases(string query);

        IConsulta UseDatabase(string query);

        IConsulta ShowTables(string query);

        IConsulta DescribeTable(string query);

    }
}
