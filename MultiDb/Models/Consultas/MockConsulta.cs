﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;

namespace MultiDb.Models
{
    public class MockConsulta : IConsulta
    {
        public ConsultaParts consultaParts = new ConsultaParts();

        public IConsulta DescribeTable(string query)
        {
            throw new NotImplementedException();
        }

        public int ExecuteQuery(string query, IDbConnection connection)
        {
            int resultado = connection.Query(query).FirstOrDefault();
            return resultado;
        }

        public IConsulta ExecuteQuery(string query)
        {
            throw new NotImplementedException();
        }

        public List<string> ShowColumns(IDbConnection connection, string Table)
        {
            List<string> Columns = connection.Query<string>("describe {0};", Table).ToList();
            return Columns;
        }

        public List<string> ShowDatabases(IDbConnection connection)
        {
            List<string> Databases = connection.Query<string>("show databases;").ToList();
            return Databases;
        }

        public IConsulta ShowDatabases(string query)
        {
            throw new NotImplementedException();
        }

        public List<string> ShowTables(IDbConnection connection)
        {
            List<string> Tables = connection.Query<string>("show tables;").ToList();
            return Tables;
        }

        public IConsulta ShowTables(string query)
        {
            throw new NotImplementedException();
        }

        public string ShowVersion(IDbConnection connection)
        {
            string version = connection.Query("select version();").FirstOrDefault();
            return version;
        }

        public int UseDatabase(IDbConnection connection, string Database)
        {
            int resultado = connection.Execute("Use {0};", Database);
            return resultado;
        }

        public IConsulta UseDatabase(string query)
        {
            throw new NotImplementedException();
        }
    }
}

