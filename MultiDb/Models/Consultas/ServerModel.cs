﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultiDb.Models.Consultas
{
    public class ServerModel
    {
        public string Version { get; set; }

        public List<string> Database { get; set; }
    }
}
