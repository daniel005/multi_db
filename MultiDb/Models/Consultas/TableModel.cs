﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultiDb.Models.Consultas
{
    public class TableModel
    {
        public string TableName { get; set; }

        public List<string> Column { get; set; }

    }
}
