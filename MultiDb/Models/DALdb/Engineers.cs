﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MultiDb.Models.UsersDb
{
    public class Engineers
    {
        public int id { get; set; }
        public string port { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public String ConnectionString { get; set; }
        public String Database { get; set; }
        
    }

}

