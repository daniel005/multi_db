﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultiDb.Models.InformacionUsuarios
{
    public class Direccion
    {
        public int IdDireccion { get; set; }

        public string Calle { get; set; }

        public int Numero { get; set; }

        public string Sector { get; set; }

        public string Ciudad { get; set; }

        public string Pais { get; set; }

        public int IdUsuario { get; set; }
    }
}
