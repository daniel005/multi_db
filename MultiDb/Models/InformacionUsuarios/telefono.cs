﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultiDb.Models.InformacionUsuarios
{
    public class Telefono
    {
        public int IdUsuario { get; set; }

        public int IdTelefono { get; set; }

        public int Numero { get; set; }
    }
}
