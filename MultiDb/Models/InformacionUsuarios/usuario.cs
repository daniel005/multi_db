﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultiDb.Models
{
    public class Usuario
    {
        public int IdUsuario { get; set; }

        public string PrimerNombre { get; set; }

        public string SegundoNombre { get; set; }

        public string ApellidoPaterno { get; set; }

        public string ApellidoMaterno { get; set; }

        public string Cedula { get; set; }

        public string Correo { get; set; }

        public string CuentaUsuario { get; set; }

        public string Password { get; set; }
    }
}
